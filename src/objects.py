from trainerbase.gameobject import GameInt
from trainerbase.memory import Address

from memory import player_base_pointer


player_base_address = Address(player_base_pointer)

max_hp = GameInt(player_base_address + [0x20])
hp = GameInt(player_base_address + [0x2C])
