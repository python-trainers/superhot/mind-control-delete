from trainerbase.main import run

from gui import run_menu
from injections import update_player_base_pointer


if __name__ == "__main__":
    run(run_menu, on_gui_initialized_hook=update_player_base_pointer.inject)
