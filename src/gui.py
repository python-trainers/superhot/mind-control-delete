from trainerbase.gui import GameObjectUI, SeparatorUI, SpeedHackUI, add_components, simple_trainerbase_menu

from objects import hp, max_hp


@simple_trainerbase_menu("SUPERHOT: MIND CONTROL DELETE", 685, 250)
def run_menu():
    add_components(
        GameObjectUI(hp, "HP", set_hotkey="F1", default_setter_input_value=50),
        GameObjectUI(max_hp, "Max HP", set_hotkey="F1", default_setter_input_value=50),
        SeparatorUI(),
        SpeedHackUI(default_factor_input_value=50.0),
    )
