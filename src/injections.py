from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.memory import AOBScan

from memory import player_base_pointer


update_player_base_pointer = AllocatingCodeInjection(
    AOBScan("8B 49 2C 3B C1 0F 8E", multiple_result_index=-1),
    f"""
        mov [{player_base_pointer}], ecx

        mov ecx, [ecx + 0x2C]
        cmp eax, ecx
    """,
    original_code_length=5,
)
